%define next_el 0

%macro colon 2
    %ifid %2
        %2: dq next_el
        %define next_el %2
    %else
        %error "second argument not id"
    %endif
    %ifstr %1
        db %1, 0
    %else
        %error "first argument not string"
    %endif
%endmacro

