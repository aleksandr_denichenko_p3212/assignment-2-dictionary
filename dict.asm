extern string_equals
global find_word

%define PTR_SIZE 8

section .text

find_word:
	test rdi, rdi
	je .exit0
        test rsi, rsi
        je .exit0
    .loop:
	push rdi
	push rsi
	lea rsi, [rsi+PTR_SIZE]
	call string_equals	
	pop rsi
	pop rdi
	test rax, rax		
	jnz .access
	mov rsi, [rsi]		
	test rsi, rsi
	jnz .loop	
	
.exit0:
	xor rax, rax
	ret
.access:
	mov rax, rsi
	ret

