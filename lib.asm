global exit
global string_length
global print_err
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


%define EXIT  60

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT
    syscall
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor     eax, eax
    cmp     BYTE [rdi], 0
    je      .return
.loop:
    inc     rax
    cmp     BYTE [rdi+rax], 0
    jne     .loop
    ret
.return:
    ret

print_err:
	mov rsi, 2
	push rdi
	push rsi
	call string_length
	pop rdi
	pop rsi
	mov rdx, rax
	mov rax, 1
	syscall
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA  
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    syscall
    pop rdi
    ret
; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi    
; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    push rbp
    mov rbp, rsp
    mov rax, rdi
    mov rbx, 1
    mov rcx, 10
    dec rsp
    mov byte [rsp], 0
    .loop:
        xor rdx, rdx
        div rcx
        add dl, '0'
        dec rsp
        inc rbx
        mov [rsp], dl
        test rax, rax
        jnz .loop
    mov rdi, rsp
    call print_string
    leave
    pop rbx
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals: 
    xor rax, rax
    .loop:
        mov cl, byte [rsi]
        cmp byte [rdi], cl
        jne .noteq
        test cl, cl
        jz .eq
        inc rsi
        inc rdi
        jmp .loop
    .noteq:
        ret
    .eq:
        inc rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rbx
    push rbp
    push r12

    xor rbx, rbx ;counter
    mov rbp, rsi
    mov r12, rdi

    .skip_space_symbols:
        call read_char
        cmp  al, ' '
        je   .skip_space_symbols
        cmp  al, `\t`
        je   .skip_space_symbols
        cmp  al, `\n`
        je   .skip_space_symbols
    jmp .st_loop
    .loop:
        cmp rbx, rbp    
        jge .fail   
        call read_char
    .st_loop: 
        cmp al, ' '
        je .skip
        cmp al, `\t`
        je .skip
        cmp al, `\n`
        je .skip

        mov [r12 + rbx], al
        test al, al
        jz .gg
        inc rbx
        jmp .loop
    .gg:
        mov rax, r12
        mov rdx, rbx

        pop r12
        pop rbp
        pop rbx
        ret
    .fail:
        xor rax, rax
        pop r12
        pop rbp
        pop rbx
        ret
    .skip:
        test rbx, rbx
        jz .loop
        jmp .gg
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8,  r8
    mov rsi,  10
.loop:
    mov cl,  [rdi+r8]
    cmp cl,  '0'
    jl  .return
    cmp cl,  '9'
    jg  .return
    sub cl,  '0'
    mul rsi
    add rax, rcx
    inc r8
    jmp .loop
.return:
    mov rdx, r8
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    je .neg
    cmp byte [rdi], '+'
    je .pos
    jne parse_uint
	.neg:
		inc rdi
    .pos:
		call parse_uint
		test rdx, rdx
		jz .fail
		inc rdx
		neg rax
		ret
	.fail:
		xor rax, rax
		ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        jae .fail
        mov r11b, byte [rdi + rax]
        mov byte [rsi + rax], r11b
        test r11b, r11b
        jz .success
        inc rax
        jmp .loop
    .fail:
        xor rax, rax
    .success:
    ret

