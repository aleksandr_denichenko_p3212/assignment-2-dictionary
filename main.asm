%include "words.inc" 
%include "lib.inc"
%include "dict.inc"

%define BUFFER_SIZE 256
%define POINTER_SIZE 8

section .rodata
   ; start_message: db "enter key:", 0
    overflow_message: db "overflow", 0
    not_found_message: db "not found", 0
    separator: db ": ", 0

section .bss
    buffer: resb BUFFER_SIZE

section .text
global _start

_start:
   ; mov rdi, start_message
    ;call print_string
.read:
    mov rdi, buffer
    mov rsi, BUFFER_SIZE 
    call read_word
    push rdx                       
    test rax,rax
    jz .overflow
    mov rdi, rax
    mov rsi, key3 
    call find_word
    test rax,rax
    jz .not_found
.print:
    add rax, POINTER_SIZE
    mov rdi, rax
    push rdi
    call print_string
    mov rdi, separator
    call print_string
    pop rdi
    pop rdx
    inc rdi
    add rdi, rdx
    call print_string
    xor rdi,rdi
    call exit
.overflow:
    mov rdi, overflow_message
    jmp .err_ex
.not_found:
    mov rdi, not_found_message
.err_ex:
    call print_err
    mov rdi, 1
    pop rdx
    call exit

