ASM=nasm
ASMFLAGS=-felf64
LD=ld
PY=python

main: main.o lib.o dict.o
	$(LD) -o $@ $^
	rm *.o
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

test:
	@$(PY) test.py


.PNONY: clean
clean:
	rm *.o
