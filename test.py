import subprocess
import unittest

def run_program(input_text):
    try:
        result = subprocess.run(
            ['./main'], 
            input=input_text,
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=True
        )
        return result.stdout
    except subprocess.CalledProcessError as e:
        return e.stderr

class TestProgram(unittest.TestCase):
    def test_key1(self):
        input_text = "key1"
        expected_output = "key1: valueForKey1"
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())

    def test_key2(self):
        input_text = "key2"
        expected_output = "key2: valueForKey2"
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())

    def test_key3(self):
        input_text = "key3"
        expected_output = "key3: valueForKey3"
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())

    def test_key4(self):
        input_text = "key4"
        expected_output = "not found"
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())

    def test_long_key(self):
        input_text = "aaaafsdlkag;lkjfshglkjdsahfhsadhgkdfkajfdkdsafkhdsfkdsalkfsadajddhdhdhdhdhdhhdhdhdhdhfsadfhsajfdkjhsdkjhfjhsadkajfkjhsdlkfkjhdsahfa;lskhdlkjsadlkgjsalkdjflakdfalhfkjhglksadghdsaflkhdafdsaglksadjf;lkjdsflhsadf;lkdsa;lfksd;ghsdjfha;lkf;ldsakf;dsakflkdsahflksadf;lkdsag;lkdsag;lsakdflkdsahflkdsajfdsfkdsag;lkdsagldsglkdsjhgslkjahgdskjfhsaflkslkjfdsfdfdfda;lksdf;lkdsaflksjdafdaflkdsfalkdjfasjdkl;jf;akldsjf;alkdsjfjaaaaaaaжщуууууууууууууууууууууууaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\n"
        expected_output = "overflow"
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())

if __name__ == "__main__":
    unittest.main()

